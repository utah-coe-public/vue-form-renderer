if(process.env.NODE_ENV == 'production'){
    module.exports = {
        configureWebpack: {
            externals: {
                'vue': 'Vue',
                'bootstrap/dist/css/bootstrap.css': 'bootstrap',
            }
        }
    }
}


module.exports.transpileDependencies =  [
    'vuetify'
  ]

