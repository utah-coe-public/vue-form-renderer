# vue-form-renderer

> Renders an html form from a JSON object built by vue-form-builder


### Opt into validation
To opt into validation add the {validate: true} setting
```
<form-renderer :prefill="prefill" :form_json="form_json"  v-on:form_answers="saveForm" ref="form" :settings="{validate:true}"> </form-renderer>
```
This will enable the form renderer to display error messages for required fields.  
It is still the **responsibility of the parent** component to check validity of the form prior to submit.  
For required fields that can be done by accesing the **isValid()** method of the form-renderer utilizing **$refs**

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
