import Vue from 'vue'

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/less/font-awesome.less';
import 'vue-wysiwyg/dist/vueWysiwyg.css';
import _ from 'lodash';
import wysiwyg from 'vue-wysiwyg';
import vSelect from 'vue-select';
import Vuelidate from 'vuelidate';
import tinymce from 'vue-tinymce-editor';
import Vuetify from 'vuetify/lib'

Vue.config.productionTip = false

Vue.use(wysiwyg);
Vue.use(Vuelidate);
Vue.use(Vuetify);
Vue.component('v-select', vSelect);
Vue.component('tinymce', tinymce);

import FormRenderer from './components/FormRenderer.vue'

export {FormRenderer};