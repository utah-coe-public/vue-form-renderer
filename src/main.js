import Vue from 'vue'
import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/less/font-awesome.less';
import 'vue-wysiwyg/dist/vueWysiwyg.css';
import wysiwyg from 'vue-wysiwyg';
import vSelect from 'vue-select';
import Vuelidate from 'vuelidate';
import tinymce from 'vue-tinymce-editor';
import Vuetify from 'vuetify/lib'

Vue.config.productionTip = false


Vue.use(wysiwyg);
Vue.use(Vuelidate);
Vue.use(Vuetify);

Vue.component('v-select', vSelect);
Vue.component('tinymce', tinymce);

new Vue({
  render: h => h(App)
}).$mount('#app')
